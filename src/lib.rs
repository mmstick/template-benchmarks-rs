#[macro_use]
extern crate horrorshow;
extern crate criterion;
#[macro_use]
extern crate fomat_macros;
#[macro_use]
extern crate markup;
extern crate askama;
extern crate yarte;

pub mod askama_bench;
pub mod fmt;
pub mod fomat;
pub mod horrorshow_bench;
pub mod markup_bench;
pub mod std_write;
pub mod yarte_bench;
