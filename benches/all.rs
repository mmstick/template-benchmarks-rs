#[macro_use]
extern crate criterion;
extern crate template_benchmarks_rs;

use criterion::{Criterion, Fun};
use template_benchmarks_rs::{
    askama_bench, fmt, fomat, horrorshow_bench, markup_bench, std_write, yarte_bench,
};

fn big_table(c: &mut Criterion) {
    c.bench_functions(
        "Big table",
        vec![
            Fun::new("Askama", |b, i| askama_bench::big_table(b, i)),
            Fun::new("fmt", |b, i| fmt::big_table(b, i)),
            Fun::new("fomat", |b, i| fomat::big_table(b, i)),
            Fun::new("Horrorshow", |b, i| horrorshow_bench::big_table(b, i)),
            Fun::new("Markup", |b, i| markup_bench::big_table(b, i)),
            Fun::new("Yarte", |b, i| yarte_bench::big_table(b, i)),
            Fun::new("write", |b, i| std_write::big_table(b, i)),
        ],
        100,
    );
}

fn teams(c: &mut Criterion) {
    c.bench_functions(
        "Teams",
        vec![
            Fun::new("Askama", |b, i| askama_bench::teams(b, i)),
            Fun::new("fmt", |b, _| fmt::teams(b)),
            Fun::new("fomat", |b, i| fomat::teams(b, i)),
            Fun::new("Horrorshow", |b, i| horrorshow_bench::teams(b, i)),
            Fun::new("Markup", |b, i| markup_bench::teams(b, i)),
            Fun::new("Yarte", |b, _| yarte_bench::teams(b)),
            Fun::new("write", |b, _| std_write::teams(b)),
        ],
        0,
    );
}

criterion_group!(benches, big_table, teams);
criterion_main!(benches);
