# Rust template engine benchmarks 
This is a fork designed for stable 

- [askama][askama]: type-safe, compiled Jinja-like templates for Rust
- [fmt][fmt]: Formats the value using the given formatter.
- [fomat][fomat]: alternative syntax for print/write/format-like macros with a small templating language
- [horrorshow][horrorshow]: a templating library written in rust macros
- [markup][markup]: A blazing fast, type-safe template engine for Rust.
- [write!][write]: the std library `write!` macro
- [yarte][yarte]: **Y**et **A**nother **R**ust **T**emplate **E**ngine, and is a handlebars-like template engine

[askama]: https://github.com/djc/askama
[fmt]: https://doc.rust-lang.org/std/fmt/trait.Display.html#tymethod.fmt
[fomat]: https://github.com/krdln/fomat-macros
[horrorshow]: https://github.com/Stebalien/horrorshow-rs
[markup]: https://github.com/utkarshkukreti/markup.rs
[write]: https://doc.rust-lang.org/std/macro.write.html
[yarte]: https://github.com/botika/yarte

## Results
The results are in the [report of ci][ci] 

[ci]: https://gitlab.com/botika/template-benchmarks-rs/pipelines

## Running the benchmarks
```bash
cargo bench

# for extract to file
cargo bench | grep time: > your_results
```

[violin plot]: https://en.wikipedia.org/wiki/Violin_plot
[Criterion]: https://github.com/bheisler/criterion.rs

Plots will be rendered if `gnuplot` is installed and will be available in the
`target/criterion` folder.
